<?php

use PHPUnit\Framework\TestCase;

class EdsReaderTest extends TestCase
{
    public function testCanBeInstantiatedWithValidParams()
    {
        $this->assertInstanceOf(
            Uagc\EdsReader::class,
            new Uagc\EdsReader([
                'auth_user' => 'username',
                'auth_pw' => 'a-very_str0ng:password'
            ])
        );
    }

    public function testCannotBeInstantiatedWithNoParams()
    {
        // at miminum, 'domain' must be passed
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\EdsReader();
    }

    public function testCannotBeInstantiatedWithInvalidParams()
    {
        // auth_user must be a non-empty string
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\EdsReader(['auth_user' => ['x' => 'invalid']]);

        // auth_pw must be a non-empty string
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\EdsReader(['auth_pw' => ['x' => 'invalid']]);

        // user_agent must be a string (or empty value)
        $this->setExpectedException(InvalidArgumentException::class);
        new Uagc\EdsReader(['user_agent' => ['x' => 'invalid']]);
    }

    public function testGetWorksWithValidArgs()
    {
        // this test is only accurate if these env vars are set
        if (getenv('EDS_TEST_USER') &&
            getenv('EDS_TEST_PW') &&
            getenv('EDS_TEST_ID')) {

            $er = new Uagc\EdsReader([
                'auth_user' => getenv('EDS_TEST_USER'),
                'auth_pw' => getenv('EDS_TEST_PW')
            ]);
            $person = $er->get(getenv('EDS_TEST_ID'));

            $this->assertTrue(
                is_object($person)
            );
        }
    }

    public function testGetWorksWithXmlOutput()
    {
        // this test is only accurate if these env vars are set
        if (getenv('EDS_TEST_USER') &&
            getenv('EDS_TEST_PW') &&
            getenv('EDS_TEST_ID')) {

            $er = new Uagc\EdsReader([
                'auth_user' => getenv('EDS_TEST_USER'),
                'auth_pw' => getenv('EDS_TEST_PW')
            ]);
            $person = $er->get(getenv('EDS_TEST_ID'), 'xml');

            $this->assertTrue(
                is_string($person)
            );
        }
    }

    public function testGetWorksWithDsmlOutput()
    {
        // this test only runs if these env vars are set
        if (getenv('EDS_TEST_USER') &&
            getenv('EDS_TEST_PW') &&
            getenv('EDS_TEST_ID')) {

            $er = new Uagc\EdsReader([
                'auth_user' => getenv('EDS_TEST_USER'),
                'auth_pw' => getenv('EDS_TEST_PW')
            ]);
            $person = $er->get(getenv('EDS_TEST_ID'), 'dsml');

            $this->assertTrue(
                is_string($person)
            );
        }
    }
}
