<?php

namespace Uagc;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

/**
 * EdsReader
 *
 * A client for the UA IAM Team's EDS Lookup service.
 *
 * @method object get(string $userId)
 *
 */
class EdsReader
{
    private $client;
    private $config;
    private $validModes;

    public function __construct($params = array())
    {
        if (empty($params['auth_user']) || !is_string($params['auth_user'])) {
            throw new \InvalidArgumentException('EDS user is invalid');
        }
        if (empty($params['auth_pw']) || !is_string($params['auth_pw'])) {
            throw new \InvalidArgumentException('EDS passphrase is invalid');
        }

        $userAgent = !empty($params['user_agent']) ?
            $params['user_agent'] :
            'UA Graduate College StacheReader for PHP';
        if (!is_string($userAgent)) {
            throw new \InvalidArgumentException('User-Agent is invalid');
        }

        $this->validModes = [
            'json' => [
                'output' => 'JSON',
                'url' => 'https://apps.iam.arizona.edu/web_services/edsLookup/%s.json',
            ],
            'xml' => [
                'output' => 'XML',
                'url' => 'https://apps.iam.arizona.edu/web_services/edsLookup/%s.xml',
            ],
            'dsml' => [
                'output' => 'DSML',
                'url' => 'https://eds.arizona.edu/people/%s',
            ]
        ];

        $this->config = [
            'auth_user' => $params['auth_user'],
            'auth_pw' => $params['auth_pw'],
            'mode' => 'json', // default mode is JSON over REST
            'url' => $this->validModes['json']['url'],
            'userAgent' => $userAgent
        ];

        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * Get the requested record
     *
     * @param string $userId A NetID or EMPLID
     *
     * @return object A PHP stdObject constructed by passing the response body through json_decode()
     */
    public function get($userId, $mode=null)
    {
        if (empty($userId) || !is_string($userId)) {
            throw new \InvalidArgumentException('Method get(): $userId is invalid');
        }

        $config = $this->config;
        if (!empty($mode) && !empty($this->validModes[$mode])) {
            $config = array_merge(
                $this->config,
                [
                    'mode' => $mode,
                    'url' => $this->validModes[$mode]['url']
                ]
            );
        }

        $requestUrl = sprintf(
            $config['url'],
            $userId
        );
        $auth = [
            $config['auth_user'],
            $config['auth_pw']
        ];
        $headers = [
            'User-Agent' => $this->config['userAgent']
        ];
        $response = $this->client->request(
            'GET',
            $requestUrl,
            [
                'auth' => $auth,
                'headers' => $headers
            ]
        );

        $responseStatus = $response->getStatusCode();
        $responseBody = $response->getBody();

        if ($responseStatus !== 200) {
            throw new \Exception(
                'Error ' . $responseStatus . ': ' . $responseBody
            );
        }

        if ($config['mode'] == 'json') {
            return json_decode((string) $responseBody);
        }

        return (string) $responseBody;
    }
}
