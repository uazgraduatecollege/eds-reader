# EDS Reader for PHP

A simple PHP package for interacting with UA EDS IAM services

## Requirements

Use of EDS Reader assumes [PHP ](https://php.net/) and [Composer](https://getcomposer.org/) in the environment.
EDS Reader has been tested successfully with PHP versions 5.5.9+ and 7.0+

## Installation

### Direct installation from Git
1. Clone the git repository
2. Install dependencies

```sh
$ git clone <git-repo-url> <output-dir>
$ cd <output-dir>
$ composer install
```

### Installation as an Composer Package Dependency

At minimum, the following should be in to your `composer.json` file:
```json
{
    "require": {
        "uagradcoll/eds-reader": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:uazgraduatecollege/eds-reader.git"
        }
    ]
}
```

## Usage

### EdsReader.get()

```php
// What you do here may depend on how you do autoloading.
// If installed w/Composer, this should be sufficient.
use Uagc\EdsReader;

// create a new reader object
$myEr = new EdsReader([
    // You're using environment vars for config, right?
    'auth_user' => getenv('EDS_AUTH_USER'),
    'auth_ps' => getenv('EDS_AUTH_PW'),
]);

// Do some queries
// Default is to get JSON
try {
    $person = $myEr->get('somenetid');
    print_r($person);
} catch (Exception $e) {
    echo $e->getMessage();
}

// We can also request XML
try {
    $person = $myEr->get('somenetid', 'xml');
    print_r($person);
} catch (Exception $e) {
    echo $e->getMessage();
}

// Or DSML
try {
    $person = $myEr->get('somenetid', 'dsml');
    print_r($person);
} catch (Exception $e) {
    echo $e->getMessage();
}
```

## License

Copyright (c) 2018 Arizona Board of Regents on behalf of the University of Arizona, all rights reserved


## Status

A work in-progress, may be `unstable`. Contributors welcome if you bring us a shrubbery.
